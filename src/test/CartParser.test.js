import CartParser from '../CartParser';
let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	test('tests validation of header: namings', () => {
		const input = 'Productname,Price,Quantity';
		const output = [{
			'column': 0,
			'message': 'Expected header to be named \"Product name\" but received Productname.',
			'row': 0,
			'type': 'header'
		}];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests validation of header: number of cells', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32`;
		const output = [{
			'column': -1,
			'message': 'Expected row to have 3 cells but received 2.',
			'row': 2,
			'type': 'row'
		}];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests validation of header: empty string in cell', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			,10.32,1`;
		const output = [{
			'column': 0,
			'message': 'Expected cell to be a nonempty string but received \"\".',
			'row': 2,
			'type': 'cell'
		}];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests validation of header: negative number in cell', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,-1`;
		const output = [{
			'column': 2,
			'message': 'Expected cell to be a positive number but received \"-1\".',
			'row': 2,
			'type': 'cell'
		}];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests validation of header: string when positive number is expected', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2,
			Tvoluptatem,10.32,string`;
		const output = [{
			type: 'cell',
			column: 2,
			row: 2,
			message: 'Expected cell to be a positive number but received "string".',
		}];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests parser: number of cells', () => {
		const input = `Product name,Price,Quantity
			Tvoluptatem,10.32,1
			Scelerisque lacinia,18.90,1
			Consectetur adipiscing,28.72,10
			Condimentum aliquet,13.90,1`;
		const output = [{
			type: 'row',
			row: 2,
			column: -1,
			message: 'Expected row to have 3 cells but received 1.',
		}];
		const expectedErrorMessage = 'Validation failed!';
		parser.readFile = jest.fn(() => input);
		parser.validate = jest.fn(() => output);

		expect(() => parser.parse('')).toThrowError(expectedErrorMessage);
	});

	test('tests errors when data is correct', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2`;
		const output = [];
		expect(parser.validate(input)).toEqual(output);
	});

	test('tests parse line', () => {
		const input = 'Mollis consequat,9.00,2';
		const output = {
			name: 'Mollis consequat',
			price: 9,
			quantity: 2
		};
		expect(parser.parseLine(input)).toMatchObject(output);
	});

	test('should calculate total price of items', () => {
		const cartItems = [
			{
				'id': '3e6def17-5e87-4f27-b6b8-ae78948523a9',
				'name': 'Mollis consequat',
				'price': 9,
				'quantity': 2
			},
			{
				'id': '90cd22aa-8bcf-4510-a18d-ec14656d1f6a',
				'name': 'Tvoluptatem',
				'price': 10.32,
				'quantity': 1
			}
		];
		const expectedTotalPrice = 28.32;
		expect(parser.calcTotal(cartItems)).toEqual(expectedTotalPrice);
	})

	test('should return all validation errors', () => {
		const input = `Product name,Name,Quantity
			Consectetur adipiscing,
			Condimentum aliquet,13.90,-1`;
		const output = [
			{
				'column': 1,
				'message': 'Expected header to be named \"Price\" but received Name.',
				'row': 0,
				'type': 'header'
			},
			{
				'column': -1,
				'message': 'Expected row to have 3 cells but received 2.',
				'row': 1,
				'type': 'row'
			},
			{
				'column': 2,
				'message': 'Expected cell to be a positive number but received \"-1\".',
				'row': 2,
				'type': 'cell'
			}
		];

		expect(parser.validate(input)).toEqual(output);
	});

	test('should return error: headers are missed', () => {
		const input = 'Mollis consequat,18.9,2';
		const output = [
			parser.createError(
				'header',
				0,
				0,
				'Expected header to be named "Product name" but received Mollis consequat.'
			),
			parser.createError(
				'header',
				0,
				1,
				'Expected header to be named "Price" but received 18.9.'
			),
			parser.createError(
				'header',
				0,
				2,
				'Expected header to be named "Quantity" but received 2.'
			),
		];
		expect(parser.validate(input)).toEqual(output);
	});
});

describe('CartParser - integration test', () => {
	test('should return parsed data', () => {
		const input = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,18.90,1
			Consectetur adipiscing,28.72,10
			Condimentum aliquet,13.90,1`;

		parser.readFile = jest.fn(() => input);

		const output = {
			items: [
				{
					name: "Mollis consequat",
					price: 9,
					quantity: 2,
				},
				{
					name: "Tvoluptatem",
					price: 10.32,
					quantity: 1,
				},
				{
					name: "Scelerisque lacinia",
					price: 18.9,
					quantity: 1,
				},
				{
					name: "Consectetur adipiscing",
					price: 28.72,
					quantity: 10,
				},
				{
					name: "Condimentum aliquet",
					price: 13.9,
					quantity: 1,
				},
			],
			total: 348.32,
		};
		expect(parser.parse(input)).toMatchObject(output);
	});
});
